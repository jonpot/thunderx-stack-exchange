from os import listdir
import os

from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.sql import SparkSession
import time

conf = SparkConf()
conf.setAppName('ConvertXML2Parquet')
sc = SparkContext(conf=conf)

spark = SparkSession(sc)

# Access the hadoop filesystem api with java.
# See https://medium.com/analytics-vidhya/accessing-hadoop-filesystem-api-with-pyspark-29cff901de51
file_system=sc._jvm.org.apache.hadoop.fs.FileSystem
path=sc._jvm.org.apache.hadoop.fs.Path
fs=file_system.get(sc._jsc.hadoopConfiguration())

dir_list=fs.globStatus(path("/user/jonpot/stackexchange/*"))

# Import each xml file in each subdir of stackexchange/
# and write to parquet file.
for dir in dir_list:
    work_dir = str(dir.getPath())
    file_list=fs.globStatus(path(work_dir + "/*.xml"))
    for file in file_list:
        in_file = str(file.getPath())
        df = spark.read.format("com.databricks.spark.xml").option("rowTag","row").load(in_file)
        out_file = work_dir + "/" + os.path.splitext(os.path.basename(in_file))[0].lower() + ".parquet"
        print("Writing " + out_file)
        df.write.parquet(out_file)
