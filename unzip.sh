#!/bin/bash

#################################################################
# Decompress archive files. Takes about 5 hours to decompress 
# 70 GB to 373 GB.

me=$(whoami)
locker_stage_dir=/nfs/locker/arcts-cavium-hadoop-stage/home/${me}
hdfs_stage_dir=/user/${me}/stackexchange

cd ${locker_stage_dir}/stackexchange
zip_files="*.7z"
#zip_files="3dprinting.meta.stackexchange.com.7z 3dprinting.stackexchange.com.7z stackoverflow.com-Badges.7z ru.stackoverflow.com.7z"

# The files from stackoverflow.com came broken into separate archives; 
# i.e. stackoverflow.com-Badges.7z, stackoverflow.com-Comments.7z, etc...
# Going to output them all in a common directory, stackoverflow.com,
# if the archive name has this prefix.
stackoverflow_prefix="stackoverflow.com-"

for f in ${zip_files}; do
  echo "Uncompressing ${f}"
  # Set output directory based on archive file name.
  if [[ "$f" == "$stackoverflow_prefix"* ]]; then
    output_dir="stackoverflow.com"
  else
    output_dir=$(basename ${f} .7z)
  fi
  # Extract, do not overwrite existing files, set output dir
  7za x ${f} -aos -o${output_dir}
done

#################################################################
# Move xml files to HDFS. Remove xml files from Locker. Takes 
# 5 hours to ingest 373 GB.

hdfs dfs -mkdir ${hdfs_stage_dir}

for dir in *; do
  if [ -d "${dir}" ]; then
    echo "Moving ${dir} to HDFS"
    hdfs dfs -put ${dir} ${hdfs_stage_dir}
    rm -f ${dir}/*.xml
  fi
done
