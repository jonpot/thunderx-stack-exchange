A collection of scripts to import Stack Exchange dataset into Cavium ThunderX HDFS.

## Usage

Here are the steps to import the data.

```
./download.sh
./unzip.sh
spark-submit --master yarn --queue arcts --verbose \
  --num-executors 60 \
  --jars /sw/dsi/noarch/spark/2.2.1/lib/spark-xml_2.11-0.5.0.jar \
  --conf "spark.pyspark.python=/sw/dsi/aarch64/centos7/python/3.7.4/bin/python3" \
  --conf "spark.pyspark.driver.python=/sw/dsi/aarch64/centos7/python/3.7.4/bin/python3" \
  ./convert2parquet.py
./move.sh
```
