#!/bin/bash

#################################################################
# Download files from torrent with transmission-cli.
# Puts all of the .7z files in your locker staging directory in 
# a directory `stackexchange`. Takes about 90 minutes to
# download 70 GB.

me=$(whoami)
stage_dir=/nfs/locker/arcts-cavium-hadoop-stage/home/${me}
torrent_location=https://archive.org/download/stackexchange/stackexchange_archive.torrent
transmission-cli ${torrent_location} --download-dir ${stage_dir}
