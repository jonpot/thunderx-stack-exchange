#!/bin/bash

#################################################################
# Moves files from hdfs staging directory to final location.

me=$(whoami)
hdfs_stage_dir=/user/${me}/stackexchange
hdfs_final_dir=/data/stackexchange

# Remove xml files then move all dirs and remaining parquet files.
hdfs dfs -rm ${hdfs_stage_dir}/*/*.xml
hdfs dfs -mv ${hdfs_stage_dir}/* ${hdfs_final_dir}

# Clean up permissions on dirs then files.
hdfs dfs -chmod 755 ${hdfs_final_dir}/*
hdfs dfs -chmod 644 ${hdfs_final_dir}/README*
hdfs dfs -chmod 755 ${hdfs_final_dir}/*/*.parquet
hdfs dfs -chmod 644 ${hdfs_final_dir}/*/*.parquet/*
